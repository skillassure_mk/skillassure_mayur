
// Display student total marks and Average and also display result

public class Main
{
public static void main(String args[]) {
	String name ="Mayur Kolhe";
	int math=90;
	int science=78;
	int english=73;
	
	float total=math + science + english;
	
	float avg= total/3;
	
	System.out.println("Total Marks:"+total);
	System.out.println("Average Marks:"+avg);
	if(math>=35 && science>=35 && english>=35) {
	  if(avg>=60) {
		  System.out.println("FIRST CLASS");
	  }
	  else if(avg>=50 && avg<60)
	       System.out.println("SECOND CLASS");
	  else if(avg>=35 && avg<50)
		  System.out.println("PASS CLASS");
	  else
		  System.out.println("FAIL");
	}
	else
		System.out.println("FAIL");
}
}
